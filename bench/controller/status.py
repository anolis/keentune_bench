# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import json
import logging

from tornado.web import RequestHandler
from bench.common.system import checkAddressAvaliable

logger = logging.getLogger('common')


class StatusHandler(RequestHandler):
    def get(self):
        """
        This function is used to handle GET requests and return a status check response.

        Args:
            self: The object itself.

        Returns:
            None

        Raises:
            None
        """
        logger.info("Get status check reuqests")
        back_json = {"status": "alive"}
        self.write(json.dumps(back_json))
        self.finish()


class AvaliableAddressHandler(RequestHandler):
    def post(self):
        """
        Process POST requests and check if addresses are available.

        Args:
            self: The class instance.

        Returns:
            None

        Raises:
            None
        """
        request_data = json.loads(self.request.body)
        agent_address = request_data['agent_address']
        logger.debug("Get address check reuqests, agent_address = {}".format(agent_address))

        if not isinstance(agent_address, list):
            agent_address = [agent_address]
        result = checkAddressAvaliable(agent_address)

        logger.debug("response address check result: {}".format(result))
        self.write(json.dumps(result))
        self.finish()