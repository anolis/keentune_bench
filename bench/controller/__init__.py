# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# import os
# import errno
# import signal
# import logging

# logger = logging.getLogger('common')

# def wait_child(signum, frame):
#     try:
#         while True:
#             childpid, status = os.waitpid(-1, os.WNOHANG)
#             if childpid == 0:
#                 break
#             logger.debug('process end, pid = {}, status = {}, exitcode = {}'.format(childpid, status, status >> 8))
        
#     except OSError as e:
#         if e.errno == errno.ECHILD:
#             logger.debug('no process left: {}'.format(e))
#         else:
#             logger.error('child process error: {}'.format(e))

# signal.signal(signal.SIGCHLD, wait_child)