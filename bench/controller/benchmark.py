# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import os
import json
import subprocess
import multiprocessing
import logging

from collections import defaultdict
from tornado.web import RequestHandler

from bench.common.config import Config
from bench.common.system import httpResponse

logger = logging.getLogger('common')

BENCH_PROC_PID = []

class BenchmarkProcess(multiprocessing.Process):
    def __init__(self,
        benchmark_cmd:str, 
        response_ip: str, 
        response_port: str,
        bench_id: int):
        """
        Initialize the BenchmarkProcess object.

        Args:
            benchmark_cmd (str): The command for benchmark.
            response_ip (str): The IP address to respond.
            response_port (str): The port to respond.
            bench_id (int): The ID of the benchmark.

        Returns:
            None
        """
        super(BenchmarkProcess, self).__init__()

        self.response_ip = response_ip
        self.response_port = response_port
        self.bench_id = bench_id

        benchmark_cmd_list = benchmark_cmd.split()
        benchmark_cmd_list[1] = os.path.join(
            Config.BENCHMARK_PATH, benchmark_cmd_list[1])
        self.benchmark_cmd = " ".join(benchmark_cmd_list)

        self.proc = subprocess.Popen(
            args   = self.benchmark_cmd,
            shell  = True,
            stderr = subprocess.PIPE,
            stdout = subprocess.PIPE
        )
        logger.info("create benchmark subprocess, cmd = {}, subpid = {}".format(self.benchmark_cmd, self.proc.pid))

    def _parseBenchmarkResult(self, benchmark_result: str):
        """
        Parse the benchmark result string and return a dictionary containing the parsed results.

        Args:
            benchmark_result (str): The benchmark result string.

        Returns:
            dict: A dictionary containing the parsed benchmark results.

        Raises:
            None

        Examples:
            >>> benchmark_result = "metric1=10, metric2=20, metric3=30"
            >>> _parseBenchmarkResult(benchmark_result)
            {'metric1': 10.0, 'metric2': 20.0, 'metric3': 30.0}
        """
        benchmark_result_dict = {}
        
        for equation in benchmark_result.split(","):
            name  = equation.split("=")[0].strip()
            value = equation.split("=")[1].strip()
            benchmark_result_dict[name] = float(value)

        return benchmark_result_dict

    def runbenchmark(self):
        """
        Function to run benchmark and parse the results.
    
        Returns:
            tuple: A tuple containing two elements, where the first element is a boolean value indicating if the benchmark was successful, and the second element is a dictionary representing the benchmark results.
        """
        benchmark_result = defaultdict(list)

        try:
            stdoutdata, stderrdata = self.proc.communicate()
            stdoutdata = stdoutdata.decode('UTF-8', 'strict').strip()
            stderrdata = stderrdata.decode('UTF-8', 'strict').strip()

            if stderrdata != "":
                logger.error("benchmark running stderr: stderr = {stderr}".format(stderr = stderrdata))
                return False, "benchmark running stderr: stderr = {stderr}".format(stderr = stderrdata)

            if stdoutdata == "":
                logger.error("benchmark result error: no output")
                return False, "benchmark result error: no output"

            try:
                benchmark_res = self._parseBenchmarkResult(stdoutdata)
                for k in benchmark_res.keys():
                    benchmark_result[k].append(benchmark_res[k])

            except Exception as e:
                logger.error("wrong benchmark output format of '{}': {}".format(stdoutdata, e))
                return False, "wrong benchmark output format of '{}': {}".format(stdoutdata, e)

        except Exception as e:
            logger.error("benchmark running error: {}".format(e))
            return False, "benchmark running error: {}".format(e)
        
        logger.info("benchmark running sucess: {}".format(dict(benchmark_result)))
        return True, dict(benchmark_result)

    def run(self):
        """
        This function runs the benchmark and sends the response to the client.

        Args:
            None

        Returns:
            None

        Raises:
            None
        """
        logger.debug("benchmark runing: pid = {}, subpid = {}".format(
            self.pid, self.proc.pid))
        
        # Run the benchmark and get the result
        suc, res = self.runbenchmark()
        response_data = {
            "suc": suc, 
            "msg": res,
            "bench_id": self.bench_id
        }

        logger.debug("response benchmark running result to {response_ip}:{response_port} : {response_data}".format(
            response_port = self.response_port,
            response_ip = self.response_ip,
            response_data = response_data
        ))

        # Send the response to the client
        httpResponse(response_data, self.response_ip, self.response_port)
        os._exit(0)


class BenchmarkHandler(RequestHandler):
    def post(self):
        """
        Handle POST request.

        Parse benchmark_cmd, resp_ip, resp_port, and bench_id parameters from the request body.
        Create a BenchmarkProcess object and start it.
        Record the process ID to the BENCH_PROC_PID list.
        Record log information and return a success message.

        Args:
            self: The instance of the class.

        Returns:
            None.
        """
        request_data = json.loads(self.request.body)

        p = BenchmarkProcess(
            benchmark_cmd = request_data["benchmark_cmd"],
            response_ip = request_data['resp_ip'],
            response_port = request_data['resp_port'],
            bench_id = request_data['bench_id'],
        )
        p.start()
        BENCH_PROC_PID.append(p.pid)
        logger.info("create benchmark process, pid = {}, {} benchmark process is runing".format(
            p.pid, BENCH_PROC_PID.__len__()))

        self.write(json.dumps({"suc" : True, "msg": "Benchmark is Runing"}))
        self.finish()


class BenchmarkTerminateHandler(RequestHandler):
    def get(self):
        """
        This function is used to get the benchmark processes and shutdown them.

        Args:
            self: The object itself.

        Returns:
            None

        Raises:
            None
        """
        while BENCH_PROC_PID.__len__() > 0:
            pid = BENCH_PROC_PID.pop()
            try:
                os.kill(pid, 9)
                logger.info("kill benchmark process, pid = {}".format(pid))
            except:
                continue            
        
        self.write(json.dumps({
            "suc" : True, "msg": "shutdown benchmark process"}))