# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import json
import requests
import subprocess
import logging

logger = logging.getLogger('common')


def proc_alive(pid):
    """
    Check if a specific process is alive
    
    Args:
        pid (int): The process ID
    
    Returns:
        bool: True if the process is alive, False otherwise
    """
    try:
        os.kill(pid, 0)
        return True
    except:
        return False


def httpResponse(response_data, response_ip, response_port):
    """
    Send HTTP response.

    Args:
        response_data (dict): The response data.
        response_ip (str): The IP address to send the response to.
        response_port (int): The port to send the response to.

    Returns:
        None

    Raises:
        None
    """
    logger.info("send response to {ip}:{port}:{data}".format(
        ip = response_ip,
        port = response_port,
        data = response_data
    ))
    try:
        requests.post(
            url = "http://{ip}:{port}/benchmark_result".format(ip = response_ip, port = response_port),
            data = json.dumps(response_data),
            timeout = 3)
    except requests.exceptions.ConnectTimeout:
        logger.warning("send response timeout: ip = {}, port = {}".format(response_ip, response_port))
        
        
def sysCommand(command: str, cwd: str = "./"):
    """
    Execute a system command and return the execution result.

    Parameters:
    command: str - The command to be executed.
    cwd: str - Optional. The current working directory (default is current directory "./").

    Returns:
    suc: bool - Indicates whether the command was executed successfully.
    out: str - Standard output result when the command is executed successfully.
    error: str - Error information when the command fails to execute.
    """
    result = subprocess.run(
        command,
        shell=True,
        close_fds=True,
        cwd=cwd,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE
    )

    suc = (result.returncode == 0)
    out = result.stdout.decode('UTF-8', 'strict').strip()
    error = result.stderr.decode('UTF-8', 'strict').strip()

    if not suc:
        return suc, error
    else:
        return suc, out


def checkAddressAvaliable(address_list: list):
    """
    Check the availability of IP addresses.

    Args:
        address_list (list): List of IP addresses.

    Returns:
        dict: Dictionary containing IP addresses and availability results.
    """
    result = {}
    for ip in address_list:
        logger.debug("ping -W 1 -c 1 {}".format(ip))
        try:
            suc, _ = sysCommand("ping -W 1 -c 1 {ip}".format(ip = ip))
            if not suc:
                logger.warning("Failed to ping {}".format(ip))
            else:
                logger.debug("Success to ping {}".format(ip))
            result[ip] = suc
        
        except Exception as e:
            logger.error("fail to ping {}: {}".format(ip, e))
            result[ip] = False

    return result
