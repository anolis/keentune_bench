# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import sys
import logging

from logging import config as logconf
from configparser import ConfigParser

LOG_CONF = {
    "version": 1,
    "formatters": {
        "std": {
            "format": "PID=%(process)d %(asctime)s %(levelname)s : %(message)s - %(filename)s:%(lineno)d"
        },
        "simple": {
            "format": "%(asctime)s %(levelname)s : %(message)s"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "simple",
            "stream": sys.stdout
        },
        "logfile": {
            "class": "logging.handlers.TimedRotatingFileHandler",
            "filename": "/var/log/keentune/bench.log",
            "interval": 1,
            "backupCount": 14,
            "level": "DEBUG",
            "formatter": "std",
        }
    },
    "loggers": {
        "common": {
            "level": "DEBUG",
            "handlers": ["console","logfile"],
            "propagate": False
        },
        "auto": {
            "level": "DEBUG",
            "handlers": ["logfile"],
            "propagate": False
        }
    },
    "root": {
        "level": "DEBUG",
        "handlers": ["console"]
    }
}

class Config:
    conf_file_path = "/etc/keentune/bench/bench.conf"
    conf = ConfigParser()
    conf.read(conf_file_path)
    print("Load config from {}".format(conf_file_path))

    LOG_CONF['handlers']['console']['level']       = conf['log']['CONSOLE_LEVEL']
    LOG_CONF['handlers']['logfile']['level']       = conf['log']['LOGFILE_LEVEL']
    LOG_CONF['handlers']['logfile']['filename']    = conf['log']['LOGFILE_PATH']
    LOG_CONF['handlers']['logfile']['interval']    = int(conf['log']['LOGFILE_INTERVAL'])
    LOG_CONF['handlers']['logfile']['backupCount'] = int(conf['log']['LOGFILE_BACKUP_COUNT'])

    logconf.dictConfig(LOG_CONF)
    logger = logging.getLogger('common')

    HOME       = conf['bench']['HOME']              # /etc/keentune/bench
    WORKSPACE  = conf['bench']['WORKSPACE']         # /var/keentune/bench
    BENCH_PORT = conf['bench']['BENCH_PORT']
    BENCHMARK_PATH = conf['bench']['BENCHMARK_PATH']# /var/keentune/bench/scripts

    logger.info("keentune-bench install path: {}".format(HOME))
    logger.info("keentune-bench workspace: {}".format(WORKSPACE))
    logger.info("keentune-bench listenting port: {}".format(BENCH_PORT))