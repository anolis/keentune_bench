# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import tornado
import os
import logging

from bench.controller.sendfile import SendfileHandler
from bench.controller.benchmark import BenchmarkHandler,BenchmarkTerminateHandler
from bench.controller.status import StatusHandler, AvaliableAddressHandler

from bench.common.config import Config

logger = logging.getLogger('common')


""" 
KeenTune-bench main function.  

KeenTune-bench running in an auxiliary environment distinguished from tunning target environment.

This environment is used for running benchmark script to target environment to calculate the performance score of target environment with specified parameter configuration
"""


def main():
    app = tornado.web.Application(handlers=[
        (r"/sendfile", SendfileHandler),
        (r"/benchmark", BenchmarkHandler),
        (r"/terminate", BenchmarkTerminateHandler),
        (r"/status", StatusHandler),
        (r"/avaliable", AvaliableAddressHandler)
    ])
    app.listen(Config.BENCH_PORT)
    logger.info("keentune-bench running")
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        os._exit(0)