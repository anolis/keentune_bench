# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
from setuptools import setup, find_packages

setup(
    name             = "keentune-bench",
    version          = "2.4.1",
    description      = "KeenTune bench unit",
    url              = "https://gitee.com/anolis/keentune_bench",
    license          = "MulanPSLv2",
    packages         = find_packages(exclude=["test"]),
    package_data     = {'bench': ['bench.conf']},
    python_requires  = '>=3.6',
    long_description = "",
    install_requires = ['tornado'],

    classifiers = [
        "Environment:: KeenTune",
        "IntendedAudience :: Information Technology",
        "IntendedAudience :: System Administrators",
        "License :: OSI Approved :: MulanPSLv2",
        "Operating System :: POSIX :: Linux",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3.6",
        "ProgrammingLanguage :: Python"
    ],

    data_files  = [
        ("/etc/keentune/bench", ["bench/bench.conf"]),
        ("/var/log/keentune",[]),
        ("/var/keentune/bench/scripts",[])
    ],

    entry_points = {
        'console_scripts': ['keentune-bench=bench.bench:main']
    }
)
